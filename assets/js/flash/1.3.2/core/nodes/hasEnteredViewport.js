/*!
 * Determine if an element is about to enter the viewport or if it is above
 * @param  {Node}    elem The element
 * @param  {Integer}    offset Offset in pixel to anticipate or delay the result
 * @return {Boolean}      Returns true if element is in the viewport
 */
flashCore.prototype.hasEnteredViewport = function (elem, offset) {
	if(!elem) {
		console.warn('elem is not set');
		return false;
	}
	offset = offset || 0;
	var distance = elem.getBoundingClientRect();
	return (distance.top <= window.innerHeight + offset && distance.top > 0) || (distance.bottom <= window.innerHeight + offset && distance.bottom >= 0);
};